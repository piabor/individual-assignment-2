--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: courses; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.courses (
    crn integer NOT NULL,
    coursename text NOT NULL,
    classtime text NOT NULL,
    instructor text NOT NULL
);


ALTER TABLE public.courses OWNER TO postgres;

--
-- Name: registeredclasses; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.registeredclasses (
    studentid integer NOT NULL,
    coursecrn integer NOT NULL
);


ALTER TABLE public.registeredclasses OWNER TO postgres;

--
-- Name: students; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.students (
    id integer NOT NULL,
    firstname text NOT NULL,
    lastname text NOT NULL,
    email text NOT NULL,
    password text NOT NULL,
    gender text,
    city text,
    country text,
    classyear integer,
    age integer,
    major text
);


ALTER TABLE public.students OWNER TO postgres;

--
-- Name: students_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.students_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.students_id_seq OWNER TO postgres;

--
-- Name: students_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.students_id_seq OWNED BY public.students.id;


--
-- Name: students id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.students ALTER COLUMN id SET DEFAULT nextval('public.students_id_seq'::regclass);


--
-- Data for Name: courses; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.courses (crn, coursename, classtime, instructor) FROM stdin;
20001	Financial Accounting	Mon-Wed 10:00 AM - 11:15 AM	Hun, Lai Chee
20002	Fundamentals of Business	Wed-Fri 08:30 AM - 09:45 AM	Hajiyev, Ayaz
20003	Entrepreneurship	Tue-Thu 10:00 AM - 11:15 AM	Ilyas, Emin
20004	Corporate Communications	Tue-Sat 06:30 PM - 07:45 PM	Hajiyev, Rustam
20005	Risk Management	Tue-Thu 01:15 PM - 02:30 PM	Rahimov, Elmar
20006	Business Law	Mon-Wed 01:15 PM - 02:30 PM	Eyvazov, Elnur
20007	Electric Circuits Design	Tue-Thu 02:45 PM - 04:00 PM	Pashazade, Pasha
20008	Intro to Computer Networks	Mon-Wed 02:45 PM - 4:00 PM	Aliyev, Nariman
20009	Computer Org. & Architecture	Thu-Sat 11:30 AM - 12:45 PM	Ganjaliyev, Fadai
20010	Web & Mobile II	Mon-Wed 11:30 AM - 12:45 PM	Jafarzade, Anar
\.


--
-- Data for Name: registeredclasses; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.registeredclasses (studentid, coursecrn) FROM stdin;
10	20001
10	20002
10	20003
10	20004
10	20005
14	20001
14	20003
14	20006
14	20007
14	20009
12	20001
12	20002
12	20003
12	20008
12	20010
\.


--
-- Data for Name: students; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.students (id, firstname, lastname, email, password, gender, city, country, classyear, age, major) FROM stdin;
12	Emil	Suleymanli	esuleymanli4845@ada.edu.az	39fea859ce2bd86cd7bb3aec8f017dded0d32c892d327df00b40c7cef2b45c2466a162fadfe39bed71e4804a420b9ee5d259a847f301f1fb0e61a68d786ac41a	male	Baku	Azerbaijan	2022	20	Business Administration
16	Gahraman	Suleymanli	emil_151@mail.ru	39fea859ce2bd86cd7bb3aec8f017dded0d32c892d327df00b40c7cef2b45c2466a162fadfe39bed71e4804a420b9ee5d259a847f301f1fb0e61a68d786ac41a	\N	\N	\N	\N	\N	\N
\.


--
-- Name: students_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.students_id_seq', 18, true);


--
-- Name: courses courses_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.courses
    ADD CONSTRAINT courses_pk PRIMARY KEY (crn);


--
-- Name: students students_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.students
    ADD CONSTRAINT students_email_key UNIQUE (email);


--
-- Name: students students_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.students
    ADD CONSTRAINT students_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

