package ada.utils;
import org.apache.commons.validator.routines.EmailValidator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class stringValidator {
    public static boolean isValidEmail(String email) {
        EmailValidator validator = EmailValidator.getInstance();
        if(validator.isValid(email)){
            return true;
        } else{
            System.out.println("Email is not valid!");
            return false;
        }
    }

    public static boolean passwordsMatch(String password1, String password2){
        if (password1.equals(password2)){
            return true;
        } else{
            System.out.println("Passwords do not match!");
            return false;
        }
    }
    public static boolean isValidPassword(String password) {

        String regex = "^(?=.*[0-9])"
                + "(?=.*[a-z])(?=.*[A-Z])"
                + "(?=\\S+$).{8,20}$";

        Pattern p = Pattern.compile(regex);
        if (password == null) {
            System.out.println("A password should be between 8 and 20 characters long and include at least one lower case, one upper case letter, and one number");
            return false;
        }
        Matcher m = p.matcher(password);
        if (m.matches()){
            return true;
        } else{
            System.out.println("A password should be between 8 and 20 characters long and include at least one lower case, one upper case letter, and one number");
            return false;
        }
    }
}
