package ada.servlet;

import ada.utils.passwordEncryption;
import ada.utils.stringValidator;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class registerValidationServlet extends HttpServlet {
    DataSource dataSource;

    @Override
    public void init() throws ServletException {
        super.init();
        Context initContext;
        try {
            initContext = new InitialContext();
            Context webContext = (Context)initContext.lookup("java:/comp/env");
            dataSource = (DataSource) webContext.lookup("jdbc/user_datasource");
        } catch (NamingException e) {
            e.printStackTrace();
        }

    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String email = req.getParameter("registerEmail");
        String password = req.getParameter("registerPassword");
        String passwordConfirm = req.getParameter("registerPasswordConfirm");
        if(stringValidator.isValidPassword(password) && stringValidator.passwordsMatch(password, passwordConfirm) && stringValidator.isValidEmail(email)) {
            try {
                Connection conn = dataSource.getConnection();
                String query = "INSERT INTO students (firstname, lastname, email, password) VALUES(?,?,?,?)";
                PreparedStatement pstmt = conn.prepareStatement(query);
                pstmt.setString(1, firstName);
                pstmt.setString(2, lastName);
                pstmt.setString(3, email);
                pstmt.setString(4, passwordEncryption.hashPassword(password));

                int x = pstmt.executeUpdate();

                if (x == 1) {
                    System.out.println("User has been successfully registered!");
                    req.setAttribute("success_message", "You have been registered successfully! Please login to your account.");
                    RequestDispatcher rd = req.getRequestDispatcher("login.jsp");
                    rd.forward(req,resp);
                }

                conn.close();

            } catch (Exception ex) {
                System.out.println("Error: " + ex);
                req.setAttribute("error_message", "A user with this email address has already been registered or there was another problem on our side. Sorry for inconvenience.");
                RequestDispatcher rd = req.getRequestDispatcher("login.jsp");
                rd.forward(req,resp);
            }
        } else{
            req.setAttribute("error_message", "There was some problem in the information you have entered. Please enter a valid email address, confirm the password, and use a password that passes the password validation.");
            RequestDispatcher rd = req.getRequestDispatcher("login.jsp");
            rd.forward(req,resp);
        }
    }
}
