package ada.servlet;

import ada.utils.passwordEncryption;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class dashboardServlet extends HttpServlet {
    DataSource dataSource;

    @Override
    public void init() throws ServletException {
        super.init();
        Context initContext;
        try {
            initContext = new InitialContext();
            Context webContext = (Context)initContext.lookup("java:/comp/env");
            dataSource = (DataSource) webContext.lookup("jdbc/user_datasource");
        } catch (NamingException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if(req.getParameter("registerFor")!=null) {
            int registeredClassId = Integer.parseInt(req.getParameter("registerFor"));
            ArrayList <String[]> result = (ArrayList<String[]>) session.getAttribute("courseList");
            ArrayList <String[]> registeredClasses = (ArrayList<String[]>) session.getAttribute("registeredList");
            boolean courseRemoved = false;
            for(int i=0; i<result.size(); i++){
                if(i==1 && courseRemoved==true){
                    i--;
                    courseRemoved=false;
                }
                if (Integer.parseInt(result.get(i)[0]) == registeredClassId) {
                    registeredClasses.add(result.get(i));
                    result.remove(i);
                    courseRemoved=true;
                    if(i>0){
                        i--;
                        courseRemoved=false;
                    }
                }
            }
            session.setAttribute("courseList", result);
            session.setAttribute("registeredList", registeredClasses);
            req.setAttribute("success_message", "You have registered to the course. Please save your changes before you exit.");
            RequestDispatcher rd = req.getRequestDispatcher("index.jsp");
            rd.forward(req,resp);

        }
        if(req.getParameter("dropFrom")!=null) {
            int dropClassId = Integer.parseInt(req.getParameter("dropFrom"));
            ArrayList <String[]> result = (ArrayList<String[]>) session.getAttribute("courseList");
            ArrayList <String[]> registeredClasses = (ArrayList<String[]>) session.getAttribute("registeredList");
            boolean courseRemoved = false;
            for(int i=0; i<registeredClasses.size(); i++){
                if(i==1 && courseRemoved==true){
                    i--;
                    courseRemoved=false;
                }
                if (Integer.parseInt(registeredClasses.get(i)[0]) == dropClassId) {
                    result.add(registeredClasses.get(i));
                    registeredClasses.remove(i);
                    courseRemoved=true;
                    if(i>0){
                        i--;
                        courseRemoved=false;
                    }
                }
            }
            session.setAttribute("courseList", result);
            session.setAttribute("registeredList", registeredClasses);
            req.setAttribute("success_message", "You have unenrolled from the course. Please save your changes before you exit.");
            RequestDispatcher rd = req.getRequestDispatcher("index.jsp");
            rd.forward(req,resp);

        }

        if(req.getParameter("saveChanges")!=null) {
            ArrayList <String[]> registeredClasses = (ArrayList <String[]>) session.getAttribute("registeredList");
            try {
                Connection conn = dataSource.getConnection();
                String query = "DELETE FROM registeredclasses WHERE studentid=?";
                PreparedStatement pstmt = conn.prepareStatement(query);
                pstmt.setInt(1, (Integer) session.getAttribute("userId"));
                pstmt.executeUpdate();
                for(int i=0; i<registeredClasses.size(); i++) {
                    query = "INSERT INTO registeredclasses (studentid, coursecrn) VALUES(?,?)";
                    PreparedStatement pstmt2 = conn.prepareStatement(query);
                    pstmt2.setInt(1, (Integer) session.getAttribute("userId"));
                    pstmt2.setInt(2, Integer.parseInt(registeredClasses.get(i)[0]));

                    int x = pstmt2.executeUpdate();

                    if (x == 1) {
                        System.out.println("User has been successfully registered for " + registeredClasses.get(i)[1]);
                    }
                }
                req.setAttribute("success_message", "You have been registered for the classes successfully!");
                RequestDispatcher rd = req.getRequestDispatcher("index.jsp");
                rd.forward(req, resp);

            } catch (Exception ex) {
                System.out.println("Error: " + ex);
                req.setAttribute("error_message", "Save failed! There was a problem on our side. Sorry for inconvenience.");
                RequestDispatcher rd = req.getRequestDispatcher("index.jsp");
                rd.forward(req, resp);
            }
        }
        if(Integer.parseInt(req.getParameter("logout"))==1){
            session.invalidate();
            req.setAttribute("success_message", "You have been logged out successfully!");
            RequestDispatcher rd = req.getRequestDispatcher("login.jsp");
            rd.forward(req,resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if(req.getParameter("savePersonal")!=null){
            try {
                Connection conn = dataSource.getConnection();
                String query = "UPDATE students SET firstname=?, lastname=?, gender=?, city=?, country=?, classyear=?, age=?, major=? WHERE id=?";
                PreparedStatement pstmt = conn.prepareStatement(query);
                pstmt.setString(1, req.getParameter("firstname"));
                pstmt.setString(2, req.getParameter("lastname"));
                pstmt.setString(3, req.getParameter("gender"));
                pstmt.setString(4, req.getParameter("city"));
                pstmt.setString(5, req.getParameter("country"));
                pstmt.setInt(6, Integer.parseInt(req.getParameter("classyear")));
                pstmt.setInt(7, Integer.parseInt(req.getParameter("age")));
                pstmt.setString(8, req.getParameter("major"));
                pstmt.setInt(9, (Integer) session.getAttribute("userId"));
                pstmt.executeUpdate();

                session.setAttribute("firstName", req.getParameter("firstname"));
                session.setAttribute("lastName", req.getParameter("lastname"));
                session.setAttribute("gender", req.getParameter("gender"));
                session.setAttribute("city", req.getParameter("city"));
                session.setAttribute("country", req.getParameter("country"));
                session.setAttribute("classYear", Integer.parseInt(req.getParameter("classyear")));
                session.setAttribute("age", Integer.parseInt(req.getParameter("age")));
                session.setAttribute("major", req.getParameter("major"));
                req.setAttribute("success_message", "Your personal information have been updated successfully!");
                RequestDispatcher rd = req.getRequestDispatcher("index.jsp");
                rd.forward(req, resp);

            } catch (Exception ex) {
                System.out.println("Error: " + ex);
                req.setAttribute("error_message", "Save failed! There was a problem on our side. Sorry for inconvenience.");
                RequestDispatcher rd = req.getRequestDispatcher("index.jsp");
                rd.forward(req,resp);
            }

        }
    }
}
