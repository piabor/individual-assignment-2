package ada.servlet;

import ada.utils.passwordEncryption;
import ada.utils.stringValidator;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class loginValidationServlet extends HttpServlet {
    DataSource dataSource;

    @Override
    public void init() throws ServletException {
        super.init();
        Context initContext;
        try {
            initContext = new InitialContext();
            Context webContext = (Context)initContext.lookup("java:/comp/env");
            dataSource = (DataSource) webContext.lookup("jdbc/user_datasource");
        } catch (NamingException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String loginEmail = req.getParameter("loginEmail");
        String loginPassword = req.getParameter("loginPassword");
        if(stringValidator.isValidPassword(loginPassword) && stringValidator.isValidEmail(loginEmail)) {
            try {
                Connection conn = dataSource.getConnection();
                String query = "SELECT * FROM students where email =?";
                PreparedStatement pstmt = conn.prepareStatement(query);
                pstmt.setString(1, loginEmail);
                ResultSet rs = pstmt.executeQuery();

                if (rs.next()) {
                    if (rs.getString("password").equals(passwordEncryption.hashPassword(loginPassword))) {
                        HttpSession session = req.getSession();
                        session.setAttribute("userId", rs.getInt("id"));
                        session.setAttribute("firstName", rs.getString("firstname"));
                        session.setAttribute("lastName", rs.getString("lastname"));
                        session.setAttribute("emailAddress", rs.getString("email"));
                        session.setAttribute("gender", rs.getString("gender"));
                        session.setAttribute("city", rs.getString("city"));
                        session.setAttribute("country", rs.getString("country"));
                        session.setAttribute("classYear", rs.getInt("classyear"));
                        session.setAttribute("age", rs.getInt("age"));
                        session.setAttribute("major", rs.getString("major"));

                        query = "SELECT * FROM courses";
                        pstmt = conn.prepareStatement(query);
                        rs = pstmt.executeQuery();
                        int columnCount = rs.getMetaData().getColumnCount();
                        ArrayList <String[]> result = new ArrayList<String[]>();
                        while(rs.next()){
                            String[] row = new String[columnCount];
                            for (int i=0; i <columnCount ; i++)
                            {
                                row[i] = rs.getString(i + 1);
                            }
                            result.add(row);
                        }

                        query = "SELECT coursecrn FROM registeredclasses where studentid=?";
                        pstmt = conn.prepareStatement(query);
                        pstmt.setInt(1, (Integer) session.getAttribute("userId"));
                        rs = pstmt.executeQuery();
                        ArrayList <Integer> registeredClassesId = new ArrayList<Integer>();
                        while(rs.next()){
                            registeredClassesId.add(rs.getInt(1));
                        }
                        ArrayList <String[]> registeredClasses = new ArrayList<String[]>();
                        boolean courseRemoved = false;
                        for(int i=0; i<result.size(); i++){
                            if(i==1 && courseRemoved==true){
                                i--;
                                courseRemoved=false;
                            }
                            for(int j=0; j<registeredClassesId.size(); j++) {
                                if (Integer.parseInt(result.get(i)[0]) == registeredClassesId.get(j)) {
                                    registeredClasses.add(result.get(i));
                                    result.remove(i);
                                    courseRemoved=true;
                                    if(i>0){
                                        i--;
                                        courseRemoved=false;
                                    }
                                }
                            }
                        }
                        session.setAttribute("courseList", result);
                        session.setAttribute("registeredList", registeredClasses);
                        RequestDispatcher rd = req.getRequestDispatcher("index.jsp");
                        rd.forward(req,resp);

                    } else{
                        req.setAttribute("error_message", "Login failed! Please verify your email address and password and try again.");
                        RequestDispatcher rd = req.getRequestDispatcher("login.jsp");
                        rd.forward(req,resp);
                    }
                } else {
                    req.setAttribute("error_message", "User with this email address could not be found! Please verify your email address and password and try again.");
                    RequestDispatcher rd = req.getRequestDispatcher("login.jsp");
                    rd.forward(req,resp);
                }

                conn.close();

            } catch (Exception ex) {
                System.out.println("Error: " + ex);
                req.setAttribute("error_message", "Login failed! There was a problem on our side. Sorry for inconvenience.");
                RequestDispatcher rd = req.getRequestDispatcher("login.jsp");
                rd.forward(req,resp);
            }
        } else{
            req.setAttribute("error_message", "Login failed! Please verify your email address and password and try again.");
            RequestDispatcher rd = req.getRequestDispatcher("login.jsp");
            rd.forward(req,resp);
        }
    }

}
