<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page session="true" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${sessionScope.userId == null}">
    <c:redirect url="login.jsp"/>
</c:if>
<html>
    <head>
        <title>Class Registration</title>
        <style>
            html,body {
                margin:0;
                padding: 0;
            }
            table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 800px;
            }

            td, th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
            }

            tr:nth-child(even) {
                background-color: #dddddd;
            }
            th, .centerButton{
                text-align:center;
            }
            ul {
                list-style-type: none;
                margin: 0;
                padding: 0;
                overflow: hidden;
                background-color: #333;
            }

            li {
                float: left;
            }

            li a, .logoutButton, #saveButton {
                display: block;
                color: white;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
            }

            li a:hover:not(.active){
                background-color: #111;
            }

            .active {
                background-color: #337AB7;
            }
            .logoutButton, #saveButton{
                border: none;
                border-radius: 0;
                font-size: 16px;
                text-align: center;
                cursor: pointer;
            }
            .logoutButton{
                background-color: #DC3545;
            }
            .logoutButton:hover{
                background-color: #CC3945;
            }
            #availableCourses, #registeredCourses{
                display: inline-table;
                margin:3.5%;
            }
            #saveButton{
                background-color: #4CAF50;
            }
            #saveButton:hover{
                background-color: #46A04A;
            }
            #personalInfoContainer input[type=text], select {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            #personalInfoContainer button{
                font-size: 16px;
                width: 100%;
                background-color: #4CAF50;
                color: white;
                padding: 14px 20px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            #personalInfoContainer button:hover {
                background-color: #45a049;
            }
            #personalInfoContainer{
                margin-top: 4%;
                width: 100%;
                display: none;
            }
            #formContainer {
                border-radius: 5px;
                background-color: #f2f2f2;
                padding: 20px;
                width: 600px;
                margin: auto;
            }
            #success-message{
                color:green;
            }
            #error-message{
                color:red;
            }
            #message-container{
                font-size: 16px;
                margin-top: 20px;
                margin-left: auto;
                margin-right: auto;
            }
            #registrationContainer{
                width: 100%;
                text-align: center;
            }
        </style>
    </head>

    <body>
        <ul>
            <li><a class="active" href="#register" id="register">Register</a></li>
            <li><a href="#personal-info" id="personal-info">Personal Information</a></li>
            <button class="logoutButton" name="logout" type="submit" form="registerClass" value="1" style="float:right">Logout</button>
            <button id="saveButton" type="submit" name="saveChanges" form="registerClass" value="1" style="float:right">Save Changes</button>
        </ul>
        <div id="registrationContainer">
            <div id="message-container">
                <span id="success-message">
                    <c:if test="${not empty success_message}">
                        <c:out value="${success_message}"/>
                    </c:if>
                </span>
                <span id="error-message">
                    <c:if test="${not empty error_message}">
                        <c:out value="${error_message}"/>
                    </c:if>
                </span>
            </div>
            <form action="dashboard" method="get" id="registerClass">
                <div id="availableCourses">
                    <h2>Available Courses for Registration</h2>
                    <table>
                        <tr>
                            <th>CRN</th>
                            <th>Course Title</th>
                            <th>Meeting Times</th>
                            <th>Instructor</th>
                            <th>Register</th>
                        </tr>
                        <c:forEach items="${sessionScope.courseList}" var="element" varStatus="loop">
                            <tr>
                                <td>${element[0]}</td>
                                <td>${element[1]}</td>
                                <td>${element[2]}</td>
                                <td>${element[3]}</td>
                                <td class="centerButton"><button type="submit" value="${element[0]}" name="registerFor">Add</button></td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div id="registeredCourses">
                    <h2>Registered Courses</h2>
                    <table>
                        <tr>
                            <th>CRN</th>
                            <th>Course Title</th>
                            <th>Meeting Times</th>
                            <th>Instructor</th>
                            <th>Unenroll</th>
                        </tr>
                        <c:forEach items="${sessionScope.registeredList}" var="element" varStatus="loop">
                            <tr>
                                <td>${element[0]}</td>
                                <td>${element[1]}</td>
                                <td>${element[2]}</td>
                                <td>${element[3]}</td>
                                <td class="centerButton"><button type="submit" value="${element[0]}" name="dropFrom">Drop</button></td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </form>
        </div>
        <div id="personalInfoContainer">
            <div id="formContainer">
                <form action="dashboard" method="POST">

                    <label for="email">Your Student Email Address</label>
                    <input type="text" id="email" name="email" value="${sessionScope.emailAddress}" disabled>

                    <label for="firstname">First Name</label>
                    <input type="text" id="firstname" name="firstname" value="${sessionScope.firstName}" placeholder="Your first name">

                    <label for="lastname">Last Name</label>
                    <input type="text" id="lastname" name="lastname" value="${sessionScope.lastName}" placeholder="Your last name">

                    <label for="gender">Gender</label>
                    <select id="gender" name="gender">
                        <option value="male" <c:if test = "${sessionScope.gender == 'male'}">selected</c:if>>Male</option>
                        <option value="female" <c:if test = "${sessionScope.gender == 'female'}">selected</c:if>>Female</option>
                        <option value="other" <c:if test = "${sessionScope.gender == 'other'}">selected</c:if>>Other</option>
                    </select>

                    <label for="age">Age</label>
                    <input type="text" id="age" name="age" value="<c:if test="${sessionScope.age != 0}">${sessionScope.age}</c:if>" placeholder="e.g. 18">

                    <label for="city">City</label>
                    <input type="text" id="city" name="city" value="${sessionScope.city}" placeholder="e.g. Baku">

                    <label for="country">Country</label>
                    <input type="text" id="country" name="country" value="${sessionScope.country}" placeholder="e.g. Azerbaijan">

                    <label for="major">Major</label>
                    <input type="text" id="major" name="major" value="${sessionScope.major}" placeholder="e.g. Business Administration">

                    <label for="classyear">Year of Class</label>
                    <input type="text" id="classyear" name="classyear" value="<c:if test="${sessionScope.classYear != 0}">${sessionScope.classYear}</c:if>" placeholder="e.g. 2022">

                    <button type="submit" name="savePersonal" value="1">Save Personal Information</button>
                </form>
            </div>
        </div>
        <script type="text/javascript">
            document.getElementById("personal-info").onclick = function(){
                document.getElementById("registrationContainer").style.display = "none";
                document.getElementById("saveButton").style.display = "none";
                document.getElementById("personalInfoContainer").style.display = "block";
                document.getElementById("register").classList.remove("active");
                document.getElementById("personal-info").classList.add("active");
            };
            document.getElementById("register").onclick = function(){
                document.getElementById("personalInfoContainer").style.display = "none";
                document.getElementById("saveButton").style.display = "block";
                document.getElementById("registrationContainer").style.display = "block";
                document.getElementById("personal-info").classList.remove("active");
                document.getElementById("register").classList.add("active");
            };
        </script>
    </body>
</html>
