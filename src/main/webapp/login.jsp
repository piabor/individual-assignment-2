<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page session="true" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${sessionScope.userId != null}">
    <c:redirect url="index.jsp"/>
</c:if>
<html>

    <head>
        <title>Authentication</title>
        <style type="text/css">
            .auth span{
                font-size: 12px;
                color: gray;
            }
            #register{
                display: none;
            }
            #loginContainer input[type=text], #loginContainer input[type=password], select {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            #loginContainer button{
                font-size: 16px;
                width: 100%;
                background-color: #4CAF50;
                color: white;
                padding: 14px 20px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            #loginContainer button:hover {
                background-color: #45a049;
            }
            #loginContainer{
                margin-top: 4%;
                width: 100%;
            }
            #formContainer {
                border-radius: 5px;
                background-color: #f2f2f2;
                padding: 20px;
                width: 650px;
                margin: auto;
            }
            #success-message{
                color:green;
            }
            #error-message{
                color:red;
            }
        </style>
    </head>

    <body>
        <div id="loginContainer">
            <div id="formContainer">
                <span id="success-message">
                    <c:if test="${not empty success_message}">
                        <c:out value="${success_message}"/>
                    </c:if>
                </span>
                <span id="error-message">
                    <c:if test="${not empty error_message}">
                        <c:out value="${error_message}"/>
                    </c:if>
                </span>
                <div class="auth" id="login">
                    <h2>Login</h2>
                    <form action="validateLogin" method="POST">
                        <label for="loginEmail">Email</label>
                        <input type="text" id="loginEmail" name="loginEmail">
                        <label for="loginPassword">Password</label>
                        <input type="password" id="loginPassword" name="loginPassword">
                        <button type="submit">Login</button>
                    </form>

                    <p>If you do not have an account, please <a id="goRegister" href="#">register</a>.</p>
                </div>
                <div class="auth" id="register">
                    <h2>Register</h2>
                    <form action="validateRegister" method="POST">
                        <label for="firstName">First Name</label>
                        <input type="text" id="firstName" name="firstName">
                        <label for="lastName">Last Name</label>
                        <input type="text" id="lastName" name="lastName">
                        <label for="registerEmail">Email</label>
                        <input type="text" id="registerEmail" name="registerEmail">
                        <label for="registerPassword">Password</label>
                        <span>(Passwords should be 8-20 characters long and contain at least one upper, one lower case letter, and one number)</span>
                        <input type="password" id="registerPassword" name="registerPassword">
                        <label for="registerPasswordConfirm">Confirm Password</label>
                        <input type="password" id="registerPasswordConfirm" name="registerPasswordConfirm">
                        <button type="submit">Register</button>
                    </form>

                    <p>If you already have an account, please <a id="goLogin" href="#">login</a>.</p>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            document.getElementById("goRegister").onclick = function(){
                document.getElementById("login").style.display = "none";
                document.getElementById("register").style.display = "block";
            };
            document.getElementById("goLogin").onclick = function(){
                document.getElementById("register").style.display = "none";
                document.getElementById("login").style.display = "block";
            };
        </script>

    </body>

</html>
